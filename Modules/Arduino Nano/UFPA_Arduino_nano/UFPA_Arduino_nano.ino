// include the library code:
#include <SoftwareSerial.h>

const byte RE = 11;
const byte DE = 10;

SoftwareSerial rs485 (12, 9);

String GP_MSG = "";
String PC_MSG = "";
float GP_Offset;
float GP_Current_PSI;

void setup() {
  Serial.begin(38400);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB
  }
  
  rs485.begin (19200);
  pinMode (RE, OUTPUT);
  pinMode (DE, OUTPUT);
  
}

void loop() {
  
  // Read from GP50
  digitalWrite (RE, HIGH); //Set to transmit
  digitalWrite (DE, HIGH);
  rs485.print("!003:SYS?\r");
  delayMicroseconds(100);
  digitalWrite (RE, LOW); //Set to receive
  digitalWrite (DE, LOW);

  for (byte i = 0; i < 200; i++) {
    delay(10);
    if (rs485.available() > 0) {
      GP_MSG = rs485.readStringUntil(13);
      if (GP_MSG.indexOf('+') >= 0) {
        GP_MSG = GP_MSG.substring(1);
      }
      // Got a response, exit loop
      break;
    }
    // Failed to get a response, show -999.99
    GP_MSG = "-999.99";
  }

  // Write to PAXI Unit
  digitalWrite (RE, HIGH); //Set to transmit
  digitalWrite (DE, HIGH);
  rs485.print("N1VC" + GP_MSG + "$");
  delayMicroseconds(100);
  digitalWrite (RE, LOW); //Set to receive
  digitalWrite (DE, LOW);
  
  if (Serial) {
    Serial.print("P:" + GP_MSG + '\r');  //Write to TX/RX lines
    if (Serial.available() > 0) {
      PC_MSG = Serial.readStringUntil(13);
      if (PC_MSG == "Zero") {
        // Zero routine
        GP_Current_PSI = GP_MSG.toFloat();
        
        digitalWrite (RE, HIGH); //Set to transmit
        digitalWrite (DE, HIGH);
        rs485.print("!003:SZ?\r");
        delayMicroseconds(100);
        digitalWrite (RE, LOW); //Set to receive
        digitalWrite (DE, LOW);

        for (byte i = 0; i < 200; i++) {
          delay(10);
          if (rs485.available() > 0) {
            GP_MSG = rs485.readStringUntil(13);
            break;
          }
        }
        if (GP_MSG.indexOf('+') >= 0) {
          GP_MSG = GP_MSG.substring(1);
        }
        GP_Offset = GP_MSG.toFloat();
        digitalWrite (RE, HIGH); //Set to transmit
        digitalWrite (DE, HIGH);
        rs485.print("!003:SZ=" + String(GP_Current_PSI - GP_Offset) + '\r');
        delayMicroseconds(100);
        digitalWrite (RE, LOW); //Set to receive
        digitalWrite (DE, LOW);
            
            
       }
     }
   }
  delay(100);
}
